var express = require('express');
var createError = require('http-errors');
var router = express.Router();

router.get('/', function(req, res, next) {
  const us = [
      {
          name:'박종혁',
          age:30
      },
      {
        name:'김진아',
        age:27
      },
  ]
  res.send({ users:us })
});

router.all('*', function(req, res, next) {
  next(createError(404, '그런 api 없다'));
});

module.exports = router;
